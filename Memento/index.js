// Editor ----------------------------------------------------------------------
class Editor {
  constructor() {
    this.state = {
      text: ""
    }
    this.careTaker = new CareTaker(this.state);
    this.addListeners();
    this.render();
  }

  // Adds listeners to GUI interactable elements
  addListeners() {
    document.getElementById('textfield').addEventListener('input', debounce(e => {
      // Updates state to the textfields value and saves state
      this.state = {...this.state, text: e.target.value};
      this.saveState();
    }, 500));

    document.getElementById('undoButton').addEventListener('click', () => {
      // Sets the state to the previous state history from this state
      this.setState(this.careTaker.get(this.careTaker.getIndex()-1));
    });

    document.getElementById('redoButton').addEventListener('click', () => {
      // Sets the state to the next state history from this state
      this.setState(this.careTaker.get(this.careTaker.getIndex()+1));
    });
  }

  // Saves the current state in CareTaker's Memento list
  saveState() {
    this.careTaker.save(this.state);
    this.render();
  }

  // Updates the state with a Memento and renders
  setState(memento) {
    if (memento) this.state = memento;
    this.render();
  }

  // Renders the history list and attaches listeners to entries
  updateHistoryList() {
    document.getElementById('historyList').innerHTML = ""; // init list
    this.careTaker.getHistory().map((memento, i) => { // for every Memento
      var newEntry = document.createElement('div'); // create div element

      // Attaches text to the div with indication of current state placement
      newEntry.innerHTML = "Entry #" + (i + 1);
      if (i == this.careTaker.getIndex()) newEntry.style.color = "#f9690e";

      // Button updates Editor's state to its specific state if clicked
      newEntry.addEventListener('click', () => {
        this.setState(this.careTaker.get(i));
      });

      // Adds the div to the list
      document.getElementById('historyList').appendChild(newEntry);
    })
  }

  render() {
    this.updateHistoryList();

    document.getElementById('textfield').value = this.state.text;

    // Disable undo button if there is no previous state in history
    var undoButton = document.getElementById('undoButton');
    if (this.careTaker.canUndo()) undoButton.disabled = false;
    else                          undoButton.disabled = true;

    // Disable redo button if there is no next state in history
    var redoButton = document.getElementById('redoButton');
    if (this.careTaker.canRedo()) redoButton.disabled = false;
    else                          redoButton.disabled = true;
  }
}


// Caretaker -------------------------------------------------------------------
class CareTaker {
  constructor(initialState = null) {
    this.mementos = initialState ? [initialState] : [];
    this.index = this.mementos.length - 1;
  }

  save(memento) {
    //  delete all "next" states if a change is being made in a previous state
    if (this.canRedo()) this.mementos.length = this.index + 1;

    this.mementos.push(memento);  // add new state entry
    this.index++;                 // increment current index
  }

  get(index) {
    this.index = index;           // set current index to the one requested
    return this.mementos[index];  // return the memento at specified index
  }

  getIndex() { return this.index; }
  getHistory() { return this.mementos; }

  canRedo() { return this.index < this.mementos.length - 1; }
  canUndo() { return this.index != 0; }
}


// Main ------------------------------------------------------------------------
function main() {
  var editor = new Editor();
}


// Helpers functions -----------------------------------------------------------

/*
  debounce
  Delays invoking the callback function until after time milliseconds
  have elapsed since the last time the debounced function was invoked.
  - Lodash Documentation
  Credit: https://gist.github.com/nmsdvid/8807205
*/
function debounce(callback, time) {
  let interval;
  return (...args) => {
    clearTimeout(interval);
    interval = setTimeout(() => {
      interval = null;
      callback(...args);
    }, time);
  };
}
