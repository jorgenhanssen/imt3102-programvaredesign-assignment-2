// Constants
const MESSAGES = ["Error", "Information", "Success"];
const INTERVAL = 4000;



var items = [];
var it = generator();

addItemsRandomly();
LogNewItems();






// Functions -------------------------------------------------------------------

// returns a new item if there are more to get; else returns null.
function* generator() {
  for (let i = 0;;)   yield (items.length-1 > i) ? items[++i] : null;
}

// Adds a random number to items at random intervals.
function addItemsRandomly() {
  items.push(MESSAGES[Math.floor(Math.random() * MESSAGES.length)]);
  setTimeout(addItemsRandomly, Math.random() * 12000);
}

// Checks if there are any new messages every second, if so log it.
function LogNewItems() {
  let next = it.next();

  if (next.value) console.log("show snackbar: " + next.value);
  else            console.log("no snackbar");

  setTimeout(LogNewItems, INTERVAL);
}
